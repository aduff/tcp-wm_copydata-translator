import socket
import threading
import socketserver
from time import sleep


class ThreadedTCPRequestHandler(socketserver.BaseRequestHandler):
    def setup(self):
        welcome_str = 'Connection opened from: %s' % str(self.client_address)
        print(welcome_str)
        self.request.sendall(b'%s\r\n' % bytes(welcome_str, encoding='utf-8'))

    def handle(self):
        while 1:
            data = self.request.recv(1024)
            if not data:
                print('Connection was closed by client.')
                break
            print('Thread: %s. Received: %s' % (threading.current_thread(), str(data)))
            self.request.sendall(b'%s\r\n' % data.upper())


class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass


def client(ip, port, message):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((ip, port))
    try:
        sock.sendall(bytes(message, encoding='utf-8'))
        response = sock.recv(1024)
        print("Received: {}".format(response))
    finally:
        sock.close()


if __name__ == "__main__":
    # Port 0 means to select an arbitrary unused port
    HOST, PORT = "localhost", 6341

    server = ThreadedTCPServer((HOST, PORT), ThreadedTCPRequestHandler)
    ip, port = server.server_address

    # Start a thread with the server -- that thread will then start one
    # more thread for each request
    server_thread = threading.Thread(target=server.serve_forever)
    # Exit the server thread when the main thread terminates
    server_thread.daemon = False
    server_thread.start()
    print("Server loop running in thread:", server_thread.name)

    while(1):
        sleep(0.5)
        print('Working...')

    #client(ip, port, "Hello World 1")
    #client(ip, port, "Hello World 2")
    #client(ip, port, "Hello World 3")

    #server.shutdown()
    #server.server_close()