import win32con
import win32api
import win32gui
import ctypes
import ctypes.wintypes
import struct
from datetime import datetime
from os import path
from array import array
import concurrent.futures
import asyncio


class CopyDataStructure(ctypes.Structure):
    _fields_ = [
        ('dwData', ctypes.wintypes.LPARAM),
        ('cbData', ctypes.wintypes.DWORD),
        ('lpData', ctypes.c_void_p)
    ]


WM_DISPLAY_TEXT = 3
MAX_TCP_CONNECTIONS = 1
P_COPY_DATA_STRUCTURE = ctypes.POINTER(CopyDataStructure)
WINDOW_CAPTION = 'Tcp2WmCopyData'

verbose = True
log_to_file = True


class WmCdListener:
    def __init__(self):
        message_map = {
            win32con.WM_COPYDATA: self.on_copy_data
        }
        win_class = win32gui.WNDCLASS()
        win_class.lpfnWndProc = message_map
        win_class.lpszClassName = 'SomeWindowClass'
        h_instance = win_class.hInstance = win32api.GetModuleHandle(None)
        class_atom = win32gui.RegisterClass(win_class)
        self.h_wnd = win32gui.CreateWindow(class_atom, WINDOW_CAPTION, 0, 0, 0,
                                           win32con.CW_USEDEFAULT, win32con.CW_USEDEFAULT,
                                           0, 0, h_instance, None)
        log_line('[WM_CD] Listener was created on WinID = 0x%X (%d)' % (self.h_wnd, self.h_wnd))
        log_line('[WM_CD] Window Caption: \'%s\'' % WINDOW_CAPTION)
        self.wm_cd_message = ''

    def on_copy_data(self, hwnd, message, wparam, lparam):
        log_line('[WM_CD] Received new message. hwnd=0x%X (%d), wparam=0x%X, lpdata=0x%X'
                 % (hwnd, message, wparam, lparam))
        p_cds = ctypes.cast(lparam, P_COPY_DATA_STRUCTURE)
        if p_cds.contents.dwData != WM_DISPLAY_TEXT:
            log_line('[WM_CD] Not WM_DISPLAY_TEXT dwData=%d cbData=0x%x lpData=0x%x'
                     % (p_cds.contents.dwData, p_cds.contents.cbData, p_cds.contents.lpData))
            return
        log_line('[WM_CD] WM_DISPLAY_TEXT: cbData=0x%x lpData=0x%x' % (p_cds.contents.cbData,
                                                                       p_cds.contents.lpData))
        message = ctypes.string_at(p_cds.contents.lpData)
        wmcd_receive(message)
        self.wm_cd_message = message.decode('utf-8')
        return 1


def log_line(string_to_log):
    timestamp = str(datetime.now())[:-3]
    str_to_log = '%s %s' % (timestamp, string_to_log)
    if log_to_file:
        f = open(path, 'a+')
        f.write('%s\n' % str_to_log)
        f.close()
    if verbose:
        print(str_to_log)


def wmcd_send(win_id, message):
    try:
        log_line('[WM_CD] Sending message \'%s\' to win_id 0x%08X' % (message, int(win_id)))
        is_window = win32gui.IsWindow(int(win_id))
        if is_window != 1:
            log_line('[WM_CD] Window with id \'%s\' doesn\'t exist' % win_id)
            return 0
        cd_struct = 'IIP'
        char_buffer = array('b', message)
        char_buffer_address = char_buffer.buffer_info()[0]
        char_buffer_size = char_buffer.buffer_info()[1]
        cds = struct.pack(cd_struct, WM_DISPLAY_TEXT, char_buffer_size, char_buffer_address)
        ret = win32gui.SendMessage(int(win_id), win32con.WM_COPYDATA, 0, cds)
        log_line('[WM_CD] Message sent. RetVal = %d' % ret)

    except ValueError:
        log_line('[WM_CD] Incorrect window_id passed: \'%s\'' % win_id)
        return 0

    return 1


def wmcd_receive(message):
    msg = str(message.decode('utf-8'))
    log_line('[WM_CD] Received message: \'%s\'' % msg)
    # TODO: Put some handler here


def execute_tcp_command(message, tcp_writer):
    global wm_cd_target_win_id
    tcp_data = ''
    wm_cd_data = ''
    try:
        command = message.strip().split(':')

        # Set Command handler
        if command[1] == 'set':

            # looking for window by caption
            if command[2] == 'caption':
                if len(command[3]):
                    wm_cd_target_win_id = win32gui.FindWindow(None, command[3])
                    if 1 == win32gui.IsWindow(int(wm_cd_target_win_id)):
                        tcp_data = '  Window \'%s\' ID=0x%X (%d) found.' % (command[3],
                                                                            wm_cd_target_win_id,
                                                                            wm_cd_target_win_id)
                        log_line('[WM_CD] New target window ID: %d' % wm_cd_target_win_id)
                    else:
                        tcp_data = '  Window \'%s\' not found.' % (command[3])
                else:
                    raise ValueError
            # looking for window by ID
            elif command[2] == 'id':
                if len(command[3]):
                    wm_cd_target_win_id = int(command[3])
                    if 1 == win32gui.IsWindow(int(wm_cd_target_win_id)):
                        tcp_data = '  Window ID=0x%X (%d) found.' % (wm_cd_target_win_id,
                                                                     wm_cd_target_win_id)
                        log_line('[WM_CD] New target window ID: %d' % wm_cd_target_win_id)
                    else:
                        tcp_data = '  Window \'%s\' not found.' % (command[3])
                else:
                    raise ValueError
            else:
                raise ValueError
        elif command[1] == 'snd':
            if len(command[2]):
                log_line('[TCP] -> [WM_CD] Forwarding wm_cd \'%s\'' % command[2])
                wm_cd_data = command[2]
            else:
                raise ValueError
        else:
            raise ValueError

    except (ValueError, IndexError):
        tcp_data = '  Error: Unknown command format: "%s"' % message.strip()

    if len(wm_cd_data):
        # wm_cd_data += '\r\n'
        print(wm_cd_data)
        ret = wmcd_send(wm_cd_target_win_id, bytes(wm_cd_data, 'utf-8'))
        if not ret:
            tcp_data += '\r\n  Error: Couldn\'t write data via WM_COPYDATA. '
            if not wm_cd_target_win_id:
                tcp_data += 'The Window ID is not set.'

    if len(tcp_data):
        tcp_data += '\r\n'
        tcp_writer.write(bytes(tcp_data, 'utf-8'))


@asyncio.coroutine
def handle_connection(reader, writer):
    global wm_cd_listener, tcp_connections_counter, wm_cd_target_win_id

    peer_name = writer.get_extra_info('peername')

    if tcp_connections_counter > MAX_TCP_CONNECTIONS - 1:
        log_line('[TCP] Attempting to open another TCP connection %s' % str(peer_name))
        data = bytes('  TCP Connection is already opened.\r\n', 'utf-8')
        writer.write(data)
    else:
        tcp_connections_counter += 1
        log_line('[TCP] Accepted connection from %s' % str(peer_name))

        if wm_cd_listener is None:
            wm_cd_listener = WmCdListener()
        data = bytes('  TCP Connection opened from %s\r\n'
                     '  WM_COPYDATA is being listened on Window ID: 0x%X (%d). '
                     '  Window Caption: "%s"\r\n'
                     % (str(peer_name), wm_cd_listener.h_wnd,
                        wm_cd_listener.h_wnd, WINDOW_CAPTION), 'utf-8')
        log_line(data)
        writer.write(data)

        while True:
            try:
                data = yield from asyncio.wait_for(reader.readline(), timeout=0.05)
                if data:
                    execute_tcp_command(data.decode('utf-8'), writer)
                    # writer.write(data)
                else:
                    log_line('[TCP] Connection from %s closed by peer' % str(peer_name))
                    break
            except concurrent.futures.TimeoutError:
                # Looking for incoming WM_CD Messages
                win32gui.PumpWaitingMessages()
                if len(wm_cd_listener.wm_cd_message):
                    log_line('[WM_CD] -> [TCP] Forwarding wm_cd \'%s\'' % data)
                    data = bytes(' >>\'%s\'\r\n' % wm_cd_listener.wm_cd_message, 'utf-8')
                    writer.write(data)
                    wm_cd_listener.wm_cd_message = ''

        tcp_connections_counter -= 1

    writer.close()


if __name__ == '__main__':
    path = path.dirname(path.abspath(__file__))
    path += datetime.strftime(datetime.now(), '\\%Y-%m-%d_%H-%M-%S_tcp2wmcd.log')

    wm_cd_listener = None
    wm_cd_target_win_id = 0
    tcp_connections_counter = 0
    loop = asyncio.get_event_loop()
    server_gen = asyncio.start_server(handle_connection, host='localhost',
                                      port=6341, backlog=MAX_TCP_CONNECTIONS)
    server = loop.run_until_complete(server_gen)
    log_line('Listening established on %s' % str(server.sockets[0].getsockname()))
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        server.close()
        loop.close()
